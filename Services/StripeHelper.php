<?php
/**
 * @author Azmi Fareed <mohamed.fareed@procept.com.au>
 * @copyright 2012-2015 Procept pty ltd
 * @link http://www.procept.com.au
 *
 */
 
/**
 * Stripe Helper
 *
 * Provides integration services for the Stripe payment gateway
 * 
 * This Service is enabled in StripeBundle/Resources/config/services.yml
 * 
 */

namespace Drillsight\StripeBundle\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drillsight\OrdersBundle\Entity\Invoice;
use Drillsight\StripeBundle\Entity\Transaction;

Class StripeHelper
{

	private $em;
	private $secretKey;
	private $stripeApi;
	private $stripeUsername;
	private $stripePassword;
	private $trialPeriod;
	private $currency;
	private $invoiceClosedStatusId;
	private $orderCompleteStatusId;

    /**
     * __construct
     *
     * @param EntityManager $entityManager Doctrine Entity Manager
	 * @param Integer $invoiceClosedStatusId Invoice Closed status ID
	 * @param Integer $orderCompleteStatusId Order Complete status ID
	 * 
     */
    public function __construct(EntityManager $entityManager,  $invoiceClosedStatusId, $orderCompleteStatusId)
    {
        $this->em = $entityManager;
		$this->invoiceClosedStatusId = $invoiceClosedStatusId;
		$this->orderCompleteStatusId = $orderCompleteStatusId;
		
		$settings = $this->em->getRepository('DrillsightSystemBundle:SystemSettings')->findSystemSettings();
		$this->secretKey = $settings->getPaymentGatewayPrivateKey();
		$this->stripeApi = $settings->getPaymentGatewayApi();
		$this->stripeUsername = $settings->getPaymentGatewayUser();
		$this->stripePassword = $settings->getPaymentGatewayPassword();
		$this->trialPeriod = $settings->getTrialPeriod();
		$this->currency = $settings->getCurrency();
  	}
	
	
	/**
     * postToStripe
     *
     * function to post data to Stripe API using cURL
     *
     * @param String $url Stripe API request URL
	 * @param Array $request Array of data to bu submitted to Stripe 
	 * @return JSON Object Response JSON object recieved from Stripe
     */
	private function postToStripe($url, $request) {
		$ch = curl_init($url);                                                      // set api url
	    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("HTTP/1.1 OK"));
	    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded"));
	    curl_setopt($ch, CURLOPT_USERPWD, "$this->stripeUsername:$this->stripePassword");                    
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $response = curl_exec($ch);
	    curl_close($ch);
		return $response;
    }
	
	
	/**
     * getFromStripe
     *
     * function to get data from Strip using cURL
     *
     * @param String $url Stripe API request URL
	 * @return JSON Object Response JSON object recieved from Stripe
     */
	private function getFromStripe($url) {
		$ch = curl_init();  
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_USERPWD, "$this->stripeUsername:$this->stripePassword");  
	    $response = curl_exec($ch);
	    curl_close($ch);
	    return $response;
    }
    
    /**
     * deleteInStripe
     *
     * function to delete data in Strip using cURL
     *
     * @param String $url Stripe API request URL
	 * @param Array $request Array of data to bu submitted to Stripe 
	 * @return JSON Object Response JSON object recieved from Stripe
     */
	private function deleteInStripe($url, $request) {
		$ch = curl_init();  
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_USERPWD, "$this->stripeUsername:$this->stripePassword");  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    return $response;
    }
	
	
	/**
     * Create Customer 
     *
     * function creates a customer in Stripe
     *
     * @param String $stripeToken Stripe Card Token ID 
	 * @param String $companyName Company Name in Drillsight
	 * @param String $companyEmail Company Email in Drillsight
	 * @return Array Stripe JSON response object
     */
	public function createStripeCustomer($stripeToken, $companyName, $companyEmail) {
		$url = "https://api.stripe.com/v1/customers?key=".$this->secretKey;
		$request = http_build_query(array("source" => $stripeToken,
											'description'=>$companyName,       //Company name is given as description for the customer in Stripe
										  'email'=>$companyEmail,));			 //Drillsight user email is given as the customer email
		$response = $this->postToStripe($url, $request);
		$customer = json_decode($response, true);
		return $customer;

	}
	
	/**
     * Update Customer 
     *
     * function updates a customer with a new credit card in Stripe
     *
     * @param String $customerId Stripe Customer ID
	 * @param String $stripeToken Stripe Card Token ID 
	 * @return Array Stripe JSON response object
     */
	public function updateCustomer($customerId, $stripeToken)  {
		$url = "https://api.stripe.com/v1/customers/$customerId?key=".$this->secretKey;
		$request = http_build_query(array("source" => $stripeToken,));			 
		$response = $this->postToStripe($url, $request);
		$customer = json_decode($response, true);
		return $customer;
	}
	
		
	
	/**
     * Get Card from Token
     *
     * function retrieves credit card details from Stripe using the card token id
     *
     * @param String $stripeToken Stripe Card Token ID 
	 * @return Array Stripe JSON response object
     */
	public function getToken($stripeToken) {
		//Retrieve token with credit card details from Stripe
		$url = "https://api.stripe.com/v1/tokens/$stripeToken?key=".$this->secretKey;
		
		$response = $this->getFromStripe($url);
		$token = json_decode($response, true);
		return $token;
	}
	
	
	/**
     * Make a Charge 
     *
     * function makes a Charge in Stripe for a customer
     *
     * @param Integer $customerId Stripe Customer ID
	 * @param Float $payableAmount - Amount to be charged
	 * @return Array Stripe JSON response object
     */
	public function createCharge($customerId, $payableAmount) {
		$url = "https://api.stripe.com/v1/charges?key=".$this->secretKey;
		$request = http_build_query(array('customer'=>$customerId, 'amount'=>$payableAmount*100, 'currency'=>$this->currency, ));
		
		$response = $this->postToStripe($url, $request);		
		$charge = json_decode($response, true); 
		if (!array_key_exists('error', $charge)) {          
			if($charge['status'] == 'failed')  // If Stripe doesn't returns an error but the payment fails, return an error object with failure message
			{
				return array(
				'error' => array('type' =>'Payment Failed',
								 'error_description' => $charge['failure_message'])
				);
			}
		}
		return $charge;
	}
	
		
	/**
     * Create subscription plan 
     *
     * function creates a subscription Plan in Stripe 
     *
     * @param String $companyId Company ID  in Drillsight
	 * @param Integer $companyName Company Name in Drillsight
	 * @param Float $payableAmount Subscription amount 
	 * @param Integer $trialPeriod Free trial period
	 * @return Array Stripe JSON response object
     */
	public function createPlan($companyId, $companyName, $payableAmount, $trialPeriod) {
		$url = "https://api.stripe.com/v1/plans?key=".$this->secretKey;
		$request = http_build_query(array('id'=>$companyId,             			// Company ID id given as the Plan Id for the company
										'amount'=>$payableAmount*100, 		   			// Payment amount in cents
										'currency'=>$this->currency, 					   			// Currency set to US Dollars
										'interval'=>'month', 				   			// Subscription recursion frequency set to monthly
										'name'=>"$companyName ".$this->currency." $payableAmount Plan",  // Company name & Subscription price given as the plan name
										'trial_period_days'=>$trialPeriod,		// Free trial period 
										'statement_descriptor'=>"Drillsight Plan", ));  // Description on customer statement
		$response = $this->postToStripe($url, $request);
		$plan = json_decode($response, true);  
		return $plan;
	}
	
	
	
	/**
     * Delete subscription plan 
     *
     * function deletes a subscription Plan in Stripe 
     *
     * @param String $planId Plan ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function deletePlan($planId) {
		$url = "https://api.stripe.com/v1/plans/$planId?key=".$this->secretKey;
		$request = http_build_query(array()); 
		$response = $this->deleteInStripe($url, $request);
		$plan = json_decode($response, true);  
		return $plan;
	}
	
	/**
     * Get subscription Plan details
     *
     * function retrieves plan details from Stripe using plan ID
     *
     * @param String $planId Plan ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function getPlan($planId) {
		//Retrieve token with credit card details from Stripe
		$url = "https://api.stripe.com/v1/plans/$planId?key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$plan = json_decode($response, true);
		return $plan;
	}
	
	
	/**
     *  Subscribe a Customer to a Plan 
     *
     * function creates a Subscription for the customer for a given plan in Stripe 
     *
     * @param String $stripeCustomerId Customer ID in Stripe
	 * @param String $planId Plan ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function createSubscription($stripeCustomerId, $planId) {
		$url = "https://api.stripe.com/v1/customers/$stripeCustomerId/subscriptions?key=".$this->secretKey;
		$request = http_build_query(array('plan'=>$planId, ));  
		$response = $this->postToStripe($url, $request);
		$subscription = json_decode($response, true);  
		return $subscription;
	}
	
	/**
     * Update Subscription 
     *
     * function updates the Subscription for the customer for a new plan in Stripe 
     *
     * @param String $stripeCustomerId Customer ID in Stripe
	 * @param String $stripeSubscriptionId Subscription ID in Stripe
	 * @param String $planId Plan ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function updateSubscription($stripeCustomerId, $stripeSubscriptionId, $planId) {
		$url = "https://api.stripe.com/v1/customers/$stripeCustomerId/subscriptions/$stripeSubscriptionId?key=".$this->secretKey;
		$request = http_build_query(array('plan'=>$planId, 
										  'prorate'=>'false'));  //Disable prorate calculation in Stripe for next subscription bill
		$response = $this->postToStripe($url, $request);
		$subscription = json_decode($response, true);  
		return $subscription;
	}
	
	
	
	/**
     * Delete Subscription 
     *
     * function terminates the Subscription of the customer in Stripe 
     *
     * @param String $stripeCustomerId Customer ID in Stripe
	 * @param String $stripeSubscriptionId Subscription ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function deleteSubscription($stripeCustomerId, $stripeSubscriptionId) {
		$url = "https://api.stripe.com/v1/customers/$stripeCustomerId/subscriptions/$stripeSubscriptionId?key=".$this->secretKey;
		$request = http_build_query(array('at_period_end'=>'false', ));  
		$response = $this->deleteInStripe($url, $request);
		$subscription = json_decode($response, true);  
		return $subscription;
	}
	
	/**
     * Get Subscription Details
     *
     * function retreives customer subscription details from Stripe 
     *
     * @param String $stripeCustomerId Customer ID in Stripe
	 * @param String $stripeSubscriptionId Subscription ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function getSubscription($stripeCustomerId, $stripeSubscriptionId) {
		//Retrieve token with credit card details from Stripe
		$url = "https://api.stripe.com/v1/customers/$stripeCustomerId/subscriptions/$stripeSubscriptionId?key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$subscription = json_decode($response, true);
		return $subscription;
	}
	

	
	/**
     * Delete Credit Card 
     *
     * function deletes the credit card details for the Stripe customer
     *
     * @param String $stripeCustomerId Customer ID in Stripe
	 * @param Sring $stripeCardId Card ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function deleteCard($stripeCustomerId, $stripeCardId) {
		$url = "https://api.stripe.com/v1/customers/$stripeCustomerId/sources/$stripeCardId?key=".$this->secretKey;
		$request = http_build_query(array( ));  
		$response = $this->deleteInStripe($url, $request);
		$card = json_decode($response, true);  
		return $card;
	}
	
	
	
	/**
     * Get Default Card Details for the Company
     *
     * function retrieves credit card details from Stripe for the company
     *
     * @param Integer $companyId Company ID in Drillsight
	 * @return Array Stripe JSON response object
     */
	public function getDefaultCard($companyId) {
		//Get company details from Drillsight
		$company = $this->em->getRepository('DrillsightCompanyBundle:Company')->findCompany($companyId);
		$stripeCustomerId = $company->getStripeCustomerId();
		$stripeCardId = $company->getStripeCardId();
		
		//Retrieve credit card details from Stripe
		$url = "https://api.stripe.com/v1/customers/$stripeCustomerId/cards/$stripeCardId?key=".$this->secretKey;
		$response = $this->getFromStripe($url);
		$card = json_decode($response, true);
		return $card;
	}
	
	
	/**
     * Make Refund
     *
     * function refunds the amount of a charge made in Stripe 
     *
     * @param String $chargeId Charge ID in Stripe
	 * @param Float $amount Refund amount
	 * @return Array Stripe JSON response object
     */
	public function createRefund($chargeId, $amount) {
		$url = "https://api.stripe.com/v1/charges/$chargeId/refunds?key=".$this->secretKey;
		$request = http_build_query(array('amount'=>$amount*100, ));  
		$response = $this->postToStripe($url, $request);
		$refund = json_decode($response, true);  
		return $refund;
	}
	
	/**
     * Get Refunds 
     *
     * function retrieves all Refunds for a specific charge in Stripe
     *
     * @param String $chargeId Charge ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function getRefunds($chargeId) {
		$url = "https://api.stripe.com/v1/charges/$chargeId/refunds?key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$refunds = json_decode($response, true);
		return $refunds;
	}
	
	
	/**
     * Get Charge Details
     *
     * function retieves details of a Charge in Stripe
     *
     * @param String $chargeId Charge ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function getCharge($chargeId) {
		$url = "https://api.stripe.com/v1/charges/$chargeId?key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$charge = json_decode($response, true);
		return $charge;
	}	
	
		
	/**
     * Get List of Charges for a Customer
     *
     * function retrieves all charges for a customer in Stripe
     *
     * @param String $customerId Stripe Customer ID
	 * @param Integer $limit No. of items to get (An Integer 0 to 100, Default value is 10)
	 * @return Array Stripe JSON response object
     */
	public function getChargesForCustomer($customerId, $limit) {
		$url = "https://api.stripe.com/v1/charges?customer=$customerId&limit=$limit&key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$charges = json_decode($response, true);
		return $charges;
	}
	
	/**
     * Get List of Charges for a Date Range
     *
     * function retrieves all charges for a given date range from Stripe
     *
     * @param String $customerId|NULL - Stripe Customer ID (could be NULL)
	 * @param DateTime $fromDate Start Date
	 * @param DateTime $toDate End Date
	 * @param Integer $limit No. of items to get (An Integer 0 to 100, Default value is 10)
	 * @return Array Stripe JSON response object
     */
	public function getChargesforDateRange($customerId, $fromDate, $toDate, $limit) {
		
		if($customerId)	  //Retrieve charges for a given customer for a Date Range
		{	
			$url = "https://api.stripe.com/v1/charges?customer=$customerId&created[gte]=".$fromDate->format('U')."&created[lte]=".$toDate->format('U')."&limit=$limit&key=".$this->secretKey;		
		}
		else 		//Retrieve charges for a given Date Range ONLY
		{
			$url = "https://api.stripe.com/v1/charges?created[gte]=".$fromDate->format('U')."&created[lte]=".$toDate->format('U')."&limit=$limit&key=".$this->secretKey;
		}		
		$response = $this->getFromStripe($url);
		$charges = json_decode($response, true);
		return $charges;
	}
	
	/**
     * Get Transaction Details
     *
     * function retrieves transaction details of a charge for given transaction Id in Stripe
     *
     * @param String $transactionId Transaction ID in Stripe
	 * @return Array Stripe JSON response object
     */
	public function getTransaction($transactionId) {
		$url = "https://api.stripe.com/v1/balance/history/$transactionId?key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$transaction = json_decode($response, true);
		return $transaction;
	}
	
	
	/**
     * Get Transaction List
     *
     * function gets the transaction details of list of charges for a date range for a company 
     *
	 * @param Company $company|NULL Company entity object (could be NULL)
	 * @param DateTime $fromDate Start Date
	 * @param DateTime $toDate End Date
	 * @param Integer $limit  No. of items to get (An Integer 0 to 100, Default value is 10)
	 * @return Array Stripe JSON response object
     */
	public function getTransactionList($company, $fromDate, $toDate, $limit) {
		if($company)
		{
			$stripeCustomerId = $company->getStripeCustomerId();
		}
		else 
		{
			$stripeCustomerId = NULL;
		}
		
		//Get cahrges list for given date range for customer
		$charge = $this->getChargesforDateRange($stripeCustomerId, $fromDate, $toDate, $limit);
		if (array_key_exists('error', $charge))  // If charge list retreival fails return error object 
		{   
			return $charge;
        }
		else 
		{
			$transactionList = array();
			$charges = $charge['data'];
			foreach ( $charges as $charge ) 
			{
				$transactionId = $charge['balance_transaction'];
				
				//Get transaction details for each charge
				$transaction = $this->getTransaction($transactionId);
				if (array_key_exists('error', $transaction))  // If transaction retreival fails return error object 
				{   
					return $transaction;
		        }
				else 
				{
			    	$company = $this->em->getRepository('DrillsightCompanyBundle:Company')->findOneBy(array('stripeCustomerId' => $charge['customer']));
					//Create an array of charges, tansactions and company name
					$transactionList[] = array('charge' => $charge,
											   'transaction' => $transaction,
											   'company' => $company->getName(), );
				}
			}
			return $transactionList;			
		}
	}


	/**
     * Build Download File
     *
     * Constructs a CSV file for download
     *
     * @param Array $transactionList The transaction list to download
     * @return String CSV content
     */
    public function buildDownloadFile($transactionList)
    {
		$fh = fopen('file.csv', 'w');
		$data = array();
		$data[] = array('Company Name',	
						'Date',
					  	'Paid Currency',
			            'Amount',
			            'Refund Currency',
			            'Refunded Amount',
			            'Net Amount Currency',
			            'Net Amount',
			            'Stripe Fee Currency',
			            'Stripe Fee Amount',
			            'GST Currency',
			            'GST Amount',
			            'Total Fee Currency',
			            'Total Fee Amount',
			            'Total Amount Currency',
			            'Total Amount', );

		// add the sensor data section
		foreach($transactionList as $transaction)
		{
			
			$data[] = array(
					$transaction->getCompany()->getName(),
		            $transaction->getDateCreated()->format('d/m/Y'),
		            strtoupper($transaction->getChargeCurrency()),
		            $transaction->getChargeAmount()/100,
		            strtoupper($transaction->getChargeCurrency()),
		            $transaction->getRefundedAmount()/100,
		            strtoupper($transaction->getTransactionCurrency()),
		            $transaction->getTransactionNetAmount()/100,
		            strtoupper($transaction->getTransactionCurrency()),
		            $transaction->getStripeFeeAmount()/100,	
		            strtoupper($transaction->getTransactionCurrency()),
		            $transaction->getGstAmount()/100,
		            strtoupper($transaction->getTransactionCurrency()),
		            $transaction->getTotalFeeAmount()/100,		            
		            strtoupper($transaction->getTransactionCurrency()),
		            $transaction->getTransactionTotalAmount()/100, 
			);
		}

		// put all the $data array into csv
		foreach($data as $line)
		{
			fputcsv($fh, $line);
		}
		$content = file_get_contents('file.csv');
		fclose($fh);

		return $content;
    }
	

	
	/**
     * Pay Invoice
     *
     * This function 
	 * - Makes a charge for the invoice amount in Stripe 
	 * - Saves transaction details in the DB
     *
     * @param Company $company Company in Drillsight being invoiced
	 * @param Float $amount Amount of the Invoice
     * @return Array|True Return true if successful or an array object of Stripe JSON response if there's an error
     */
    public function payInvoice($company, $amount) 
    {
    	$amount = round($amount, 2);
		$stripeCustomerId = $company->getStripeCustomerId();
				
		//Create a Charge in Stripe for the customer
		$charge = $this->createCharge($stripeCustomerId, $amount);
		if (array_key_exists('error', $charge)) {              // If payment fails return error object 
			return $charge;
        }
		
		//Save transaction details in DB
		$this->saveTransaction($company, $charge);

				
		//Payment was successfull			
		//###########Save Invoice payment details in Drillsight ###################
		//$this->saveInvoiceStatus($invoice, $payableAmount);	
		
		return true;
    }


		
	/**
     * Create Customer
     *
     * This function 
	 * - Creates a customer in Stripe
	 * - Retrieves credit card details for the token 
	 * - Saves Stripe Customer ID and Card ID in the Company entity
     *
     * @param Company $companyId Company in Drillsight
	 * @param String $token Stripe Card Token ID
     * @return Array|True Return true if successful or an array object of Stripe JSON response if there's an error
     */
    public function createCustomer($company, $token) {
    	//Get company details from Drillsight
		$companyName = $company->getName();
		$companyEmail = "";
		$stripeToken = $token->id;
				
		//Create a customer in Stripe
		$customer = $this->createStripeCustomer($stripeToken, $companyName, $companyEmail);
		if (array_key_exists('error', $customer)) {           // If customer creation fails return error object 
			return $customer;
        }
		else 
		{
			$stripeCustomerId = $customer['id'];
			//Retrieve credit card details for the token from Stripe
			$token = $this->getToken($stripeToken);
			if (array_key_exists('error', $token)) {           // If retreiving card details fails return error object 
	            return $token;		
	        }
			$stripeCardId = $token['card']['id'];
			//SAVE Stripe Customer ID & Stripe Card ID 
			$company->setStripeCustomerId($stripeCustomerId)
					->setStripeCardId($stripeCardId);
			$this->em->flush();
		}
		
		return true;
	}



	/**
     * Create Subscription for Company in Stripe
     *
     * This function 
 	 * - Deletes the subscription plan for the customer if exists
	 * - Creates a new subscription plan in Stripe and creates a subscription in Stripe for the customer
     *
     * @param Company $company Company in Drillsight
	 * @param Float $amount Subscription Amount in Drillsight
	 * @param Integer $free_trial_period Free Trial Period
     * @return Array|True Returns true if successful or an array object of Stripe JSON response if there's an error
     */
     public function createSubscriptionPlan($company, $amount, $free_trial_period) {
     	$amount = round($amount, 2);
		$companyId = $company->getId();
		$companyName = $company->getName();
		$stripeCustomerId = $company->getStripeCustomerId();
		$stripePlanId = $company->getStripePlanId();
		$stripeSubscriptionId = $company->getStripeSubscriptionId();
		

		if($stripePlanId)
		{
			//Delete the Plan in Stripe for the customer if exists
			$plan = $this->deletePlan($stripePlanId);
			if (array_key_exists('error', $plan)) {          // If plan creation fails return error object
				return $plan;
	        }
		}

    	//Create a Plan in Stripe for the customer
		$plan = $this->createPlan($companyId, $companyName, $amount, $free_trial_period);
		if (array_key_exists('error', $plan)) {          // If plan creation fails return error object
			return $plan;
        }
		else //If plan created successfully subscribe the customer to it
		{
			$planId = $plan['id'];
			
			if($stripePlanId)
			{
				//If customer had a subscription, Update the Subscription in Stripe for the customer to the newly created plan 
				$subscription = $this->updateSubscription($stripeCustomerId, $stripeSubscriptionId, $planId) ;
				if (array_key_exists('error', $subscription)) {            // If Subscription update fails return error object
					return $subscription;
		        }
				else {
					$subscriptionId = $subscription['id'];
				}
			}	
			else 
			{
				//If customer didn't have a subscription, Create a Subscription in Stripe for the customer for the created plan
				$subscription = $this->createSubscription($stripeCustomerId, $planId) ;
				if (array_key_exists('error', $subscription)) {            // If Subscription creation fails return error object
					return $subscription;
		        }
				else {
					$subscriptionId = $subscription['id'];
				}
			}		
		} 			
		
		//SAVE Stripe Plan ID & Stripe Subscription ID for the company 
		$company->setStripePlanId($planId);
		$company->setStripeSubscriptionId($subscriptionId);
		$this->em->flush();
		return true;
	}


	/**
     * Cancel Subscription for Company
     *
     * This function 
	 * - Deletes the subscription in Stripe for the customer
	 * - Deletes the plan in Stripe for the customer
	 * - Deletes the Stripe Plan ID in Drillsight for the Company
     *
     * @param Company $company Company entity object
     * @return Array|True Return true if successful or an array object of Stripe JSON response if there's an error
     */
    public function cancelSubscription($company) {
    	//Get company details from Drillsight
		$companyName = $company->getName();
		$stripeCustomerId = $company->getStripeCustomerId();
		$stripePlanId = $company->getStripePlanId();
		$stripeSubscriptionId = $company->getStripeSubscriptionId();
		
		if($stripeSubscriptionId)   //Make sure a subscription exists for the company
		{
			//Cancel the Subscription in Stripe for the customer
			$subscription = $this->deleteSubscription($stripeCustomerId, $stripeSubscriptionId) ;
			if (array_key_exists('error', $subscription)) {            // If Subscription creation fails return error object
				return $subscription;
	        }
			else {
				if($stripePlanId)
				{
					//Delete the Plan in Stripe for the customer if exists
					$plan = $this->deletePlan($stripePlanId);
					if (array_key_exists('error', $plan)) {          // If plan creation fails return error object
						return $plan;
			        }
				}				
			}
		}
		else {
			return array('error'=> array('type'=>'Subscription Cancellation Failed',
										 'message' => 'Company does not have a subscription to be cancelled'));							
		}
		
		//DELETE Stripe Plan ID & Stripe Subscription ID for the company
		$company->setStripePlanId(NULL);
		$company->setStripeSubscriptionId(NULL);
		$this->em->flush();
		
		return true;
	}

	
	
	/**
     * Create Coupon for an Amount  
     *
     * function creates a coupon for a given discount amount 
     *
     * @param String $couponId Coupon ID
	 * @param Integer $amountOff  Discount Amount
	 * @param String $frequency Frequency of the discount (forever, once, or repeating)
	 * @param Integer $durationMonths - Integer number of months the discount will be in effect (required only if frequency  is repeating)
	 * @return Array Stripe JSON response object
     */
	public function createAmountCoupon($couponId, $amountOff, $frequency, $durationMonths) {
		$url = "https://api.stripe.com/v1/coupons?key=".$this->secretKey;
		$request = http_build_query(array('id'=>$couponId,                 //Set Company ID as Coupon ID   
										  'amount_off'=>$amountOff,
										  'currency'=>$this->currency,
										  'duration'=>$frequency,
										  'duration_in_months'=>$durationMonths,));  
		$response = $this->postToStripe($url, $request);
		$coupon = json_decode($response, true);  
		return $coupon;
	}
	
	/**
     * Create Coupon for percentage of subscription amount 
     *
     * function creates a coupon for a given discount percentage
     *
     *@param String $couponId Coupon ID
	 * @param Integer $percentOff A positive integer between 1 and 100 that represents the discount the coupon will apply
	 * @param String $frequency Frequency of the discount (forever, once, or repeating)
	 * @param Integer $durationMonths Number of months the discount will be in effect (required only if frequency  is repeating)
	 * @return Array Stripe JSON response object
     */
	public function createPercentCoupon($couponId, $percentOff, $frequency, $durationMonths) {
		$url = "https://api.stripe.com/v1/coupons?key=".$this->secretKey;
		$request = http_build_query(array('id'=>$couponId,                  //Set Company ID as Coupon ID
										  'percent_off'=>$percentOff,
										  'duration'=>$frequency,
										  'duration_in_months'=>$durationMonths,));  
		$response = $this->postToStripe($url, $request);
		$coupon = json_decode($response, true);  
		return $coupon;
	}
	
	/**
     * Add a coupon to customer to create Discount
     *
	 * function updates a customer with a new coupon in Stripe
     *
     * @param String $customerId Stripe Customer ID
	 * @param String $coupon Stripe Coupon ID
	 * @return Array Stripe JSON response object
     */
	public function updateCustomerCoupon($customerId, $coupon)  {
		$url = "https://api.stripe.com/v1/customers/$customerId?key=".$this->secretKey;
		$request = http_build_query(array("coupon" => $coupon,));			 
		$response = $this->postToStripe($url, $request);
		$customer = json_decode($response, true);
		return $customer;
	}
	
	/**
     * Get Coupon Details
     *
     * function retrieves coupon details of the company
     *
     * @param String $couponId Coupon ID  (Which is the Company ID for that company)
	 * @return Array Stripe JSON response object
     */
	public function getCoupon($couponId) {
		//Retrieve coupon with Coupon ID from Stripe
		$url = "https://api.stripe.com/v1/coupons/$couponId?key=".$this->secretKey;		
		$response = $this->getFromStripe($url);
		$coupon = json_decode($response, true);
		return $coupon;
	}	
	
	/**
     * Delete Coupon
     *
     * function deletes the coupon in Stripe for the company
     *
     * @param String $couponId Coupon ID  (Which is the Company ID for that company)
	 * @return Array Stripe JSON response object
     */
	public function deleteCoupon($couponId) {
		$url = "https://api.stripe.com/v1/coupons/$couponId?key=".$this->secretKey;
		$request = http_build_query(array( ));  
		$response = $this->deleteInStripe($url, $request);
		$coupon = json_decode($response, true);  
		return $coupon;
	}
	
	/**
     * Delete Customer Subscription Discount
     *
     * function deletes the assigned discount for the customer in Stripe
     *
     * @param String $customerId Stripe Customer ID
	 * @return Array Stripe JSON response object
     */
	public function deleteCustomerDiscount($customerId) {
		$url = "https://api.stripe.com/v1/customers/$customerId/discount?key=".$this->secretKey;
		$request = http_build_query(array( ));  
		$response = $this->deleteInStripe($url, $request);
		$coupon = json_decode($response, true);  
		return $coupon;
	}		


	/**
     * Create Discount for a Company's Subscription
     *
     * function creates a coupon for the company in Stripe based on amount or percentage
     *
     * @param Company $company Company entity object
	 * @param Boolean $isAmount Indicates if the value supplied is an amount discount or not(percentage discount)
	 * @param Integer $value Amount or percentage of discount
	 * @param String $frequency Frequency of the discount (forever, once, or repeating)
	 * @param Integer $durationMonths Integer number of months the discount will be in effect (required only if frequency  is repeating)
	 * @return Array Stripe JSON response object
     */
	public function createDiscount($company, $isAmount, $value, $frequency, $durationMonths) {
		//Get company details from Drillsight
		$companyId = $company->getId();
		$stripeCustomerId = $company->getStripeCustomerId();
		
		if($isAmount)
		{
			$value = $value*100;
			$coupon = $this->createAmountCoupon($companyId, $value, $frequency, $durationMonths);
		}
		else
		{
			$coupon = $this->createPercentCoupon($companyId, $value, $frequency, $durationMonths);
		}
		
		
		if (array_key_exists('error', $coupon))   // If coupon creation fails return error object
		{            
			return $coupon;
        }
		else 
		{
			$couponId = $coupon['id'];
			//Create a discount for the customer with the created coupon
			$customer = $this->updateCustomerCoupon($stripeCustomerId, $couponId);
			if (array_key_exists('error', $customer))   // If Subscription creation fails return error object
			{            
				return $customer;
	        }
		}		
		return $coupon;
	}	
	
	
	/**
     * Cancel Discount 
     *
     * function deletes the assigned discount and the coupon for the company in Stripe
     *
     * @param Company $company Company entity object
	 * @return Array|True Return true if successful or an array object of Stripe JSON response if there's an error
     */
	public function cancelDiscount($company) {
		//Get company details from Drillsight
		$companyId = $company->getId();
		$stripeCustomerId = $company->getStripeCustomerId();
		

		$customer = $this->deleteCustomerDiscount($stripeCustomerId);
		if (array_key_exists('error', $customer))   // If discount deletion fails return error object
		{            
			return $customer;
        }
		else 
		{
			$coupon = $this->deleteCoupon($companyId);
			if (array_key_exists('error', $coupon))   // If coupon deletion fails return error object
			{            
				return $coupon;
	        }
		}		
		return true;
	}	




		
	/**
     * Create Invoice in Drillsight for Subscription payment in Stripe
     *
     * This function creates an Invoice in Drillsight for the invoice paid for subscription in Stripe
     *
	 * @param Array $stripeInvoice Stripe Invoice object for successful subscription payment
     * @return Invoice Invoice entity object
     */
    public function createStripeSubscriptionInvoice($stripeInvoice) {
    	//Get the company for the Stripe customer Id
		$company = $this->em->getRepository('DrillsightCompanyBundle:Company')->findOneBy(array('stripeCustomerId' => $stripeInvoice['customer']));		
		//Get the last order made for the company to save with the invoice, since an order is not created here
		$order = $this->em->getRepository('DrillsightOrdersBundle:Orders')->findOneBy(array('company' => $company), array('id' => 'DESC'));
		$user = $order->getUser();
		
		//Create an invoice for the stripe subscription payment
	    $closed_status = $this->em->getRepository('DrillsightOrdersBundle:Invoice')
	                          ->findInvoiceStatus($this->invoiceClosedStatusId);
							  
		$invoice = new Invoice();
		$invoice->setOrder($order)
				->setCompany($order->getCompany())
				->setUser($order->getUser())
				->setInvoiceDate(new \DateTime(date('Y-m-d H:i:s',$stripeInvoice['date'])))
				->setStatus($closed_status)
				->setTotal($stripeInvoice['total']/100)
				->setIsPaid(1)
				->setPaid($stripeInvoice['total']/100);
				
		$this->em->persist($invoice);
		$this->em->flush();
		
		$invoice->setInvoiceNumber(str_pad($invoice->getId(),6,"0",STR_PAD_LEFT));	// add some zeros to the start of the order ID to create Invoice Number
		$this->em->flush();
		
		//echo $invoice->getId();		
		return $invoice;	

	}



	/**
     * Check Credit Card Expiry
     *
     * This function checks if a company's credit card expiry date is within a 60 day period
     *
	 * @param Integer $companyId  Company ID in Drillsight
     * @return Array|NULL Returns Stripe card object if card is due to expire or NULL
     */
    public function checkCardExpiry($companyId) {
    	//Retrieve card details for the company from stripe 
		$card = $this->getDefaultCard($companyId);	
		
		if((int)$card['exp_year']== (int)date("Y") )
		{
			if((int)$card['exp_month'] < (int)date("m")+2 )    //If expiry year is equal to current year and expiry month is within two months, send email
			{
				// Send Email
				return $card;
			}
		}
		return null;
	}
	
	
	
	/**
     * Save Transaction Details in Drillsight DB
     *
     * This function saves transaction details of a charge made in Stripe
     *
	 * @param Company $company Company entity object
	 * @param Array $charge Charge JSON object returned by Stripe
     */
    public function saveTransaction($company, $charge) {
		$txn = $this->getTransaction($charge['balance_transaction']);
		if (array_key_exists('error', $txn)) 
		{
			$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
		}	
		else 
		{
			$transaction = new Transaction();
			$transaction
					->setCompany($company)
					->setDateCreated(new \DateTime(date('Y-m-d H:i:s',$charge['created'])))
					->setChargeId($charge['id'])						
					->setBalanceTransactionId($charge['balance_transaction'])
					->setChargeCurrency($charge['currency'])
					->setChargeAmount($charge['amount'])
					->setTransactionCurrency($txn['currency'])
					->setTransactionNetAmount($txn['net'])
					->setStripeFeeAmount($txn['fee_details'][0]['amount'])   
					->setGstAmount($txn['fee_details'][1]['amount'])
					->setTotalFeeAmount($txn['fee'])
					->setTransactionTotalAmount($txn['amount']);
					
			$this->em->persist($transaction);
			$this->em->flush();
		}
	}
	
	
	/**
     * Get Subscription Amount 
     *
     * function calculates the subscription payment amount for the company in Drillsight
     *
	 * @return $subscriptionAmount
     */
	/*public function getSubscriptionAmount($subscriptions) {
		$subscriptionAmount = 0;
		foreach ($subscriptions as $subscription) {
			if($subscription->getLimitation()->getIsEnabled()){
				$subscriptionAmount += $subscription->getLimitation()->getPrice();
			}
		}
		return $subscriptionAmount;
	}*/
	
	
	/**
     * getSubscriptionAmount 
     *
     * function calculates the subscription payment amount for the company in Drillsight
     *
	 * @return $subscriptionAmount
     */
	/*public function saveInvoiceStatus($invoice, $payableAmount) {
		                   
		$paid = $invoice->getPaid() + $payableAmount;		// Add the payment amount to already paid amount of invoice
       
	    $invoice->setPaid($paid);
	    if($paid >= $invoice->getTotal())      // Change invoice status to closed if the full amount is paid
	    {
	      	$closed_status = $this->em->getRepository('DrillsightOrdersBundle:Invoice')
	                          ->findInvoiceStatus($this->invoiceClosedStatusId);
	      	$invoice->setStatus($closed_status);
	
	      	// If the order is a drillsight subscription, then complete the Order also
	      	$order_complete_status = $this->em->getRepository('DrillsightOrdersBundle:Orders')
	                                  ->findOrderStatus($this->orderCompleteStatusId);
	
	      	$invoice->getOrder()->setStatus($order_complete_status);
	    }		
			
	    $this->em->flush();	
	}*/
		

}





