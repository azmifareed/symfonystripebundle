<?php
namespace Drillsight\StripeBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Tools\Pagination\Paginator;

class TransactionRepository extends EntityRepository
{
    /** Find Orders
	 * Find all Orders
	 * the following $filter parameters can be passed in as an array to narrow the search.
	 * @param $filter['fromDate'] - Start Date
	 * @param $filter['toDate'] - End date
 	 * @param $filter['company'] - Company search filter
	 */
    public function findTransactions($filter)
    {
    	if($filter)
		{
			$filter['fromDate'] = (isset($filter['fromDate'])) ? $filter['fromDate'] : NULL; 
			$filter['toDate'] = (isset($filter['toDate'])) ? $filter['toDate'] : NULL;
			$filter['company'] = (isset($filter['company'])) ? $filter['company'] : NULL;
		}
		
		try {   
  
			$result = array();
			$params = array();
			
			$queryString ='';
			if($filter['company']!=0)  //If query is for a company set the parameter for company ID
			{
				$queryString = ' AND c.id = '.$filter['company'];
			}

			if(!$filter['fromDate'])   //If a date range not set query only for company
			{
				$query = 'SELECT t FROM DrillsightStripeBundle:Transaction t
								JOIN t.company c 
								WHERE c.id = '.$filter['company'].' ORDER BY t.dateCreated DESC';
			}
			else   
			{
				$filter['toDate']->modify('+1 day');  // Add 1 day to end date to include the results for the End Date as well
				
				$query = 'SELECT t FROM DrillsightStripeBundle:Transaction t
								JOIN t.company c 
								WHERE t.dateCreated >= :fromDate AND t.dateCreated < :toDate '
								.$queryString.
								' ORDER BY t.dateCreated DESC';
								
				$params = array(
						'fromDate' => $filter['fromDate'],
						'toDate' => $filter['toDate']					
					);
			}
	
	        $result = $this->getEntityManager()
	            ->createQuery($query)	
				->setParameters($params)
			    ->getResult();
	

        } catch (\Doctrine\Orm\NoResultException $e) {
			$result = NULL;

        }
				
		return $result;
	}
	
}