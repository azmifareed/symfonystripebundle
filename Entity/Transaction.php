<?php

namespace Drillsight\StripeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

class Transaction
{
    protected	$id;
	protected	$company; 					// The company ID this transaction belongs to
	protected	$chargeId; 					// The Charge ID returned from Stripe
	protected	$balanceTransactionId; 		// The Balance Transaction ID returned from Stripe
	protected	$dateCreated; 				// The Date Transaction was made
	protected	$chargeCurrency; 			// The Currency in which the customer was charged
	protected	$chargeAmount; 				// The Amount charged from the customer
	protected	$transactionCurrency;		// The currency of net amount of transaction (local currency in Stripe)
	protected	$transactionNetAmount; 		// The net amount of transaction (In the local currency in Stripe)
	protected	$stripeFeeAmount; 			// The Stripe fee amount charged for this transaction (In the local currency in Stripe)
	protected	$gstAmount; 				// The GST amount charged for this transaction (In the local currency in Stripe)
	protected	$totalFeeAmount; 			// The total fee amount charged for this transaction (In the local currency in Stripe)
	protected	$transactionTotalAmount; 	// The Total amount charged for this transaction (In the local currency in Stripe)
	protected	$refundedCurrency; 			// The Currency in which the refund was made
	protected	$refundedAmount; 			// The total amount refunded

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

	public function getCompany()
	{
		return $this->company;
	}
	
	public function setCompany($company)
	{
		$this->company = $company;
		return $this;
	}
	
	public function getDateCreated()
	{
		return $this->dateCreated;
	}
	
	public function setDateCreated($dateCreated)
	{
		$this->dateCreated = $dateCreated;
		return $this;
	}
	
	public function getChargeId()
	{
		return $this->chargeId;
	}
	
	public function setChargeId($chargeId)
	{
		$this->chargeId = $chargeId;
		return $this;
	}
	
	public function getBalanceTransactionId()
	{
		return $this->balanceTransactionId;
	}
	
	public function setBalanceTransactionId($balanceTransactionId)
	{
		$this->balanceTransactionId = $balanceTransactionId;
		return $this;
	}
	
	public function getChargeCurrency()
	{
		return $this->chargeCurrency;
	}
	
	public function setChargeCurrency($chargeCurrency)
	{
		$this->chargeCurrency = $chargeCurrency;
		return $this;
	}
	
	public function getChargeAmount()
	{
		return $this->chargeAmount;
	}
	
	public function setChargeAmount($chargeAmount)
	{
		$this->chargeAmount = $chargeAmount;
		return $this;
	}	
	
	public function getTransactionCurrency()
	{
		return $this->transactionCurrency;
	}
	
	public function setTransactionCurrency($transactionCurrency)
	{
		$this->transactionCurrency = $transactionCurrency;
		return $this;
	}
	
	public function getTransactionNetAmount()
	{
		return $this->transactionNetAmount;
	}
	
	public function setTransactionNetAmount($transactionNetAmount)
	{
		$this->transactionNetAmount = $transactionNetAmount;
		return $this;
	}		
	
	public function getStripeFeeAmount()
	{
		return $this->stripeFeeAmount;
	}
	
	public function setStripeFeeAmount($stripeFeeAmount)
	{
		$this->stripeFeeAmount = $stripeFeeAmount;
		return $this;
	}
	
	public function getGstAmount()
	{
		return $this->gstAmount;
	}
	
	public function setGstAmount($gstAmount)
	{
		$this->gstAmount = $gstAmount;
		return $this;
	}
	
	public function getTotalFeeAmount()
	{
		return $this->totalFeeAmount;
	}
	
	public function setTotalFeeAmount($totalFeeAmount)
	{
		$this->totalFeeAmount = $totalFeeAmount;
		return $this;
	}
	
	public function getTransactionTotalAmount()
	{
		return $this->transactionTotalAmount;
	}
	
	public function setTransactionTotalAmount($transactionTotalAmount)
	{
		$this->transactionTotalAmount = $transactionTotalAmount;
		return $this;
	}
	
	public function getRefundedCurrency()
	{
		return $this->refundedCurrency;
	}
	
	public function setRefundedCurrency($refundedCurrency)
	{
		$this->refundedCurrency = $refundedCurrency;
		return $this;
	}
	
	public function getRefundedAmount()
	{
		return $this->refundedAmount;
	}
	
	public function setRefundedAmount($refundedAmount)
	{
		$this->refundedAmount = $refundedAmount;
		return $this;
	}
	
}




