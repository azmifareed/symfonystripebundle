# README #

This repository contains the Symfony2 bundle created for integrating the Stripe payment gateway to the Symfony2 web application developed by Procept (Pty) Ltd. 

### Contribution ###

* All of the files in this repository were created by myself
* The TestController and the Index views were used for testing purposes. 

### Functionality ###

* Make communication between Stripe REST API and Symfony using cURL
* Create/edit/delete Stripe customers in Stripe
* Create/edit/delete credit card details in Stripe
* Create/delete subscription plans in Stripe
* Create/edit/delete subscriptions in Stripe
* Setup Stripe Webhooks to create an invoice in the web application when an automatic subscription payment is received in Stripe and email a PDF of the invoice to the customer
* Setup Stripe Webhooks to email the customer when a payment is failed
* Retrieve transaction details for a customer in Stripe
* Create refunds for customers in Stripe
* Create coupons and discounts for customers in Stripe

### Contact ###

* For information contact Azmi at mafareed@gmail.com