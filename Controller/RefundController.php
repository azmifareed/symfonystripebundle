<?php
namespace Drillsight\StripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Drillsight\SystemBundle\Entity\Link;

class RefundController extends Controller
{

/**
     * Create New Refund
     *
     * This is a test function for development
     *
     * @param $request
 	 * @param $portal	
     * @return Response
     */
    public function newAction(Request $request, $portal, $id, $companyId) {
		// Add Links to content header
		$links = new ArrayCollection();
        $links->add(new Link("Back", "_".$portal."_stripe_transaction", array('id'=>$id, 'companyId' => $companyId),NULL,NULL,NULL,"ic_action_back_small.png"));
		
		
		//Get transaction details from Stripe		
		$stripe_helper = $this->get('stripe_helper');
		$transaction = $stripe_helper->getTransaction($id);
		if (array_key_exists('error', $transaction)) //Return to transaction list page if an error occurs
		{
			$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
			return $this->render('DrillsightStripeBundle:Index:transaction.html.twig', array(
	            'transaction' => NULL, 'companyId' => $companyId,
	        ));		
		}
		
		//Get the charge details from Stripe
		$charge = $stripe_helper->getCharge($transaction['source']);
		if (array_key_exists('error', $charge)) {		//If Stripe returns an error display error
			$this->get('session')->getFlashBag()->add('error', $charge['error']['message']);
		}
		
		//Get the refunds made for this charge  from Stripe
		$refunds = $stripe_helper->getRefunds($transaction['source']);
		if (array_key_exists('error', $refunds)) {		//If Stripe returns an error display error
			$this->get('session')->getFlashBag()->add('error', $refunds['error']['message']);
		}	       
		
		//Create form to make a new refund
		$formData = array();			
		$form = $this->createFormBuilder($formData)	
            ->add('amount', 'text', array(
                'data' => ($charge['amount']-$charge['amount_refunded'])/100, 
                'attr' => array('class'=> "form-control"), 
                'required'=> true,
            ))
            ->add('submitRefund', 'submit', array('label' => 'Submit Refund', 'attr' => array('class'=> "btn btn-primary")))
            ->getForm();
			
		
		$form -> handleRequest($request);	
		if ($form->isValid())   //If a new refund is submitted process the refund
		{
			$formData = $form->getData();
						
			//Make the refund for the submitted amount
			$refund = $stripe_helper->createRefund($transaction['source'], $formData['amount']); 
			
			if (array_key_exists('error', $refund)) 	//If Stripe returns an error display error
			{		
				$this->get('session')->getFlashBag()->add('error', $refund['error']['message']);
			}
			else 
			{
				$this->get('session')->getFlashBag()->add('notice', "Refund was successfull! ");
				
				$em = $this->getDoctrine()->getManager();
    			$trx = $em->getRepository('DrillsightStripeBundle:Transaction')->findOneBy(array('balanceTransactionId' => $id)); 
    			$trx->setRefundedAmount($refund['amount']);
				$em->flush();
				
			}
			
			//Get updated charge details  from Stripe to display
			$charge = $stripe_helper->getCharge($transaction['source']);
			if (array_key_exists('error', $charge)) 	//If Stripe returns an error display error
			{		
				$this->get('session')->getFlashBag()->add('error', $charge['error']['message']);
			}
			
			//Get updated refund details for the charge from Stripe  to display
			$refunds = $stripe_helper->getRefunds($transaction['source']);
			if (array_key_exists('error', $refunds)) 	//If Stripe returns an error display error
			{		
				$this->get('session')->getFlashBag()->add('error', $refunds['error']['message']);
			}
	
			//Return to transaction view page
			return $this->render('DrillsightStripeBundle:Transaction:view.html.twig', array(
	            'transaction' => $transaction, 'refunds' => $refunds, 'charge' => $charge, 'companyId' => $companyId, 'links' => $links, 
	        ));
		}		
		
				
        return $this->render('DrillsightStripeBundle:Refund:new.html.twig', array(
            'form' => $form->createView(), 'transaction' => $transaction, 'charge' => $charge, 'companyId' => $companyId, 'links' => $links, 
        ));

    }
	
}

	