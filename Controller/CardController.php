<?php
namespace Drillsight\StripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CardController extends Controller
{
	/**
     * Update Credit Card Details
     *
     * This function updates the existing credit card details of the company in Stripe
     *
     * @param Request $request - Submitted form data
	 * @param Request $companyId - ID of the Company
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function editAction(Request $request, $companyId) {
		//Use the Helper service
		$stripe_helper = $this->get('stripe_helper');
		
		$message="";
		$error = FALSE;	
	
		$data = $request->request->all();        //Get form data
	    if (count($data)>0) { 
	        $stripeToken = $data['stripeToken'];
			
			$em = $this->getDoctrine()->getManager();          // call doctrine entity manager
			$company = $em->getRepository('DrillsightCompanyBundle:Company')->findCompany($companyId);
			$stripeCustomerId = $company->getStripeCustomerId();
			$stripeCardId = $company->getStripeCardId();
			
			//Delete the existing card for customer 
			$result = $stripe_helper->deleteCard($stripeCustomerId, $stripeCardId);
			if (array_key_exists('error', $result)) {
				$error = TRUE;
			}
			else {
				//Update the stripe customer with the new card
				$result = $stripe_helper->updateCustomer($stripeCustomerId, $stripeToken);
				if (array_key_exists('error', $result)) {
					$error = TRUE;
				}
				else {
					//Retrieve card details 
					$token = $stripe_helper->getToken($stripeToken);
					if (array_key_exists('error', $token)) 
					{            
			            $error = TRUE;	
			        }
					$stripeCardId = $token['card']['id'];
				}
			}
			
			if($error) {
				$this->get('session')->getFlashBag()->add('error', "Error".$result['error']['message']);
				return $this->redirect($this->generateUrl('_customer_account_card_edit'));
			}
			else {
				//SAVE new Card ID in the Company entity
				$company->setStripeCardId($stripeCardId);
				$em->flush();
				$this->get('session')->getFlashBag()->add('notice', "Card details successfully updated!");
				return $this->redirect($this->generateUrl('_customer_account'));
			}
	    }


		return $this->render('DrillsightStripeBundle:Card:edit.html.twig', array(
        	'message' => $message, 
    	));
    }
}




	