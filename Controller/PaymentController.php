<?php
namespace Drillsight\StripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\Common\Collections\ArrayCollection;
use Drillsight\SystemBundle\Entity\Link;
use Doctrine\ORM\EntityManager;
use Drillsight\StripeBundle\Entity\Transaction;

class PaymentController extends Controller
{

	
	/**
     * Make a manual payment
     *
     * This function 
	 * - Provides a page with the Stripe payment form
	 * - Makes a charge for the payment amount in Stripe 
	 * - The credit card details are NOT saved for the company
     *
     * @param Request $request - Submitted form data
	 * @param Request $companyId - ID of the Company
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function manualPayAction(Request $request, $portal, $companyId) 
    {
    	// Add Links to content header
		$links = new ArrayCollection();
        $links->add(new Link("Back", "_".$portal."_company_view", array('id'=>$companyId),NULL,NULL,NULL,"ic_action_back_small.png"));
		
		//Use the Helper service
		$stripe_helper = $this->get('stripe_helper');	

		
		$data = $request->request->all();        //Get form data
	    if (count($data)>0) { 
	        $stripeToken = $data['stripeToken'];
			
			$em = $this->getDoctrine()->getManager();
	        $company = $em->getRepository('DrillsightCompanyBundle:Company')->findCompany($companyId); 
			$stripeCustomerId = $company->getStripeCustomerId();
			$amount = $data['payment'];          //Get Amount submitted from the form
		
			//Call function in Stripe Helper to make the charge for amount
			$charge = $stripe_helper->createCharge($stripeCustomerId, $amount);

			if (array_key_exists('error', $charge)) 
			{
				$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
			}	
			else 
			{					
				$this->get('session')->getFlashBag()->add('notice', 'Payment was successfully completed!');
				
				//Save transaction details in DB
				$stripe_helper->saveTransaction($company, $charge);
				
			}
			
		}
		return $this->render('DrillsightStripeBundle:Pay:manual_payment.html.twig', array(
        	'companyId' => $companyId, 'links' => $links, 
    	));	
	}

}



