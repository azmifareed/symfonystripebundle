<?php
namespace Drillsight\StripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class WebhookController extends Controller
{
	/**
     * Process WebHook Events
     *
     * function recives the webhook events from stripe and processes some of the event responses
     *
     * @param $request
     * @return Response
     */
    public function indexAction(Request $request) {
    	$response = file_get_contents('http://192.168.16.61/stripe/webhook/');
		
    	$response = '{
  "id": "evt_103MXP2kbIgHtIBFeIFqPxp7",
  "created": 1430193530,
  "livemode": false,
  "type": "invoice.payment_succeeded",
  "data": {
    "object": {
      "date": 1390486769,
      "id": "in_103MXP2kbIgHtIBFmaa4U4Xq",
      "period_start": 1430193530,
      "period_end": 1430193530,
      "lines": {
        "object": "list",
        "count": 1,
        "url": "/v1/invoices/in_103MXP2kbIgHtIBFmaa4U4Xq/lines",
        "data": [
          {
            "id": "sub_68hG6biQTLjQ0k",
            "object": "line_item",
            "type": "subscription",
            "livemode": false,
            "amount": 12000,
            "currency": "usd",
            "proration": false,
            "period": {
              "start": 1430193530,
              "end": 1432785530
            },
            "quantity": 1,
            "plan": {
              "interval": "month",
              "name": "Test $120 Plan",
              "created": 1430193529,
              "amount": 12000,
              "currency": "usd",
              "id": "52",
              "object": "plan",
              "livemode": false,
              "interval_count": 1,
              "trial_period_days": null,
              "metadata": {}
            },
            "description": null,
            "metadata": null
          }
        ]
      },
      "subtotal": 12000,
      "total": 1200,
      "customer": "cus_61EWTcKOCohlnP",
      "object": "invoice",
      "attempted": true,
      "closed": true,
      "paid": true,
      "livemode": false,
      "attempt_count": 0,
      "amount_due": 1200,
      "currency": "usd",
      "starting_balance": 0,
      "ending_balance": 0,
      "next_payment_attempt": null,
      "charge": "ch_15wPryIsrpqIOiiZWoOYrzI3",
      "discount": null,
      "application_fee": null,
      "subscription": "sub_68hG6biQTLjQ0k"
    }
  },
  "object": "event",
  "pending_webhooks": 1,
  "request": "iar_3MXPlBDPK90rso"
}';
		
		$result = json_decode($response, true);
		
		
		$stripe_helper = $this->get('stripe_helper');
		$em = $this->getDoctrine()->getManager();
		
		switch ($result['type']) {
			
			case 'invoice.payment_succeeded':
			/*
			 * If the event was a successful subscription payment
			 * - Create an invoice in Drillsight
			 * - Save transaction details in Drillsight
			 * - Create a PDF of the invoice 
			 * - Email the PDF invoice to the company user
			 * - Check if the company credit card is due to expire and send an email to the user
			 */
			 
			 	
				//Create invoice in Drillsight
				$invoice = $stripe_helper->createStripeSubscriptionInvoice($result['data']['object']);
				
				$company = $invoice->getCompany();
				
				$charge = $stripe_helper->getCharge($result['data']['object']['charge']);

				if (!array_key_exists('error', $charge)) 
				{
					//Save transaction details in Drillsight DB
					$stripe_helper->saveTransaction($company, $charge);
				}	
				
				//Get Start and End dates of the new subscription
				$subscriptionStart = $result['data']['object']['lines']['data'][0]['period']['start'];
				$subscriptionEnd = $result['data']['object']['lines']['data'][0]['period']['end'];
				
				$user = $invoice->getUser();
				$subscriptions = $company->getOngoingSubscriptions();
				
				//Update the subscription dates for each module for the company
				foreach ($subscriptions as $subscription) {
					$subscription->setEndDate(new \DateTime(date('Y-m-d H:i:s', $subscriptionEnd)));
					$em->flush();
				}				
				
				
				$settings = $em->getRepository('DrillsightSystemBundle:SystemSettings')->findSystemSettings();
				$currency = $settings->getCurrency();
				
				//Create the inoice PDF file with the twig template using the KNP Snappy bundle
				$html = $this->renderView('DrillsightStripeBundle:PDF:invoice_template.html.twig', array(
				    'invoice' => $invoice,
				    'subscriptions' => $subscriptions,
				    'subscriptionStart' => $subscriptionStart,
				    'subscriptionEnd' => $subscriptionEnd,
				    'currency' => $currency,
				));				
				$content = new Response(
							    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
							    200,
							    array(
							        'Content-Type'          => 'application/pdf',
							        'Content-Disposition'   => 'attachment; filename="invoice.pdf"'
							    )
							);

				//Email the PDF invoice to the user
				$filename = 'invoice.pdf';
				// create attachment PDF file
				$attachment = \Swift_Attachment::newInstance($content, $filename);				
				
				$fromEmail = $em->getRepository('DrillsightSystemBundle:SystemSettings')->findSystemSettings()->getOutboundEmailAddress();
				// Send Email
				$message = \Swift_Message::newInstance()
				    ->setSubject('Your new invoice')
				    ->setFrom(array($fromEmail => 'Drillsight'))
				    ->setTo($user->getEmail())
				    ->setContentType("text/html")
				    ->setBody(
				        $this->renderView('DrillsightStripeBundle:Email:invoice_email.html.twig',
				            array('to_name' => $user->getName(),
				                'invoice' => $invoice,
				            )
				        ))
				    ->attach($attachment);
				
				$this->get('mailer')->send($message);
				
				
				
				/*
				 * Check if the company credit card is due to expire and send an email
				 */
				if($card = $stripe_helper->checkCardExpiry($company->getId()))
				{
					$message = \Swift_Message::newInstance()
				    ->setSubject('Your credit card is due to expire')
				    ->setFrom(array($fromEmail => 'Drillsight'))
				    ->setTo($user->getEmail())
				    ->setContentType("text/html")
				    ->setBody(
				        $this->renderView('DrillsightStripeBundle:Email:card_expiry_email.html.twig',
				            array('to_name' => $user->getName(),
				                'card' => $card,
				            )
				        ));
				
					$this->get('mailer')->send($message);
				}
				
				$message = 'Event: invoice.payment_succeeded. Invoice:'.$invoice->getinvoiceNumber().'. Email sent.';
				return $this->render('DrillsightStripeBundle:Webhook:webhook_response.html.twig', array(
		            'message' => $message,
		        ));
				
				break;
				
				
			case 'invoice.payment_failed':
			/*
			 * If the event was a failure in subscription payment
			 * - Email the company user the details
			 * - After three attempt DO SOMETHING
			 */	
				
				//Get the company for the Stripe customer Id
				$stripeCustomerId = $result['data']['object']['customer'];
				$company = $em->getRepository('DrillsightCompanyBundle:Company')->findOneBy(array('stripeCustomerId' => $stripeCustomerId));
				//Get the last order made for the company to save with the invoice, since an order is not created here
				$order = $em->getRepository('DrillsightOrdersBundle:Orders')->findOneBy(array('company' => $company), array('id' => 'DESC'));
				$user = $order->getUser();
				
				//Get company card details to attach to email
				$card = $stripe_helper->getDefaultCard($company->getId());
				
				$fromEmail = $em->getRepository('DrillsightSystemBundle:SystemSettings')->findSystemSettings()->getOutboundEmailAddress();
				// Send Email
				$message = \Swift_Message::newInstance()
				    ->setSubject('Payment Failure')
				    ->setFrom(array($fromEmail => 'Drillsight'))
				    ->setTo($user->getEmail())
				    ->setContentType("text/html")
				    ->setBody(
				        $this->renderView('DrillsightStripeBundle:Email:payment_fail_email.html.twig',
				            array('to_name' => $user->getName(),
				            'card' => $card,
				            )
				        ));
				
				$this->get('mailer')->send($message);
				
				if($result['data']['object']['attempt_count']==3)
				{
					//This is the final payment failure and need to restrict access for the company 
				}
				
				$message = 'Event: invoice.payment_failed. Email sent.';
				return $this->render('DrillsightStripeBundle:Webhook:webhook_response.html.twig', array(
		            'message' => $message,
		        ));
				
				break;
			
			default:				
				return $this->render('DrillsightStripeBundle:Webhook:webhook_response.html.twig', array(
		            'message' => $message,
		        ));
		}

    }
	
}


	