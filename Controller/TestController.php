<?php
namespace Drillsight\StripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TestController extends Controller
{

    /**
     * Index
     *
     * This is a test function for development
     *
     * @param $portal
     * @return Response
     */
    public function indexAction($portal, $companyId) {

        $message = 'this is the stripe bundle';
		
		$stripe_helper = $this->get('stripe_helper');
		$em = $this->getDoctrine()->getManager();
	    $company = $em->getRepository('DrillsightCompanyBundle:Company')->findCompany($companyId); 
		$stripeCustomerId = $company->getStripeCustomerId();
		$result = $stripe_helper->getCompanyTransactionList($company);
				
		echo json_encode($result);
		
        return $this->render('DrillsightStripeBundle:Index:index.html.twig', array(
            'message' => $message,
        ));

    }

	/**
     * Pay
     *
     * This function reteives information from the Invoice class and also gets the Stripe card token
     *
     * @param $request, $invoiceid
     * @return Response
     */
	public function payAction(Request $request, $invoiceId) {
		$message="";
		//Use the Helper service
		$stripe_helper = $this->get('stripe_helper');
		
		
		
		
		
		
		//Retreive Payable Amount from the current Invoice created in Drillsight
		$em = $this->getDoctrine()->getManager();                    
		$invoice = $em->getRepository('DrillsightOrdersBundle:Invoice')->findInvoice($invoiceId); 
		$payableAmount = $invoice->getTotal();	
		
		
	
		//Retreive other relevant information to display
		$company = $invoice->getCompany();
		$companyName = $company->getName();
		
		$result = $stripe_helper->cancelDiscount($company);
		if (is_array($result)) {
			if (array_key_exists('error', $result)) {
				$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
			}
		}
		
		
		/*$company->setStripeCustomerId(NULL)
				->setStripeCardId(NULL)
				->setStripePlanId(NULL)
				->setStripeSubscriptionId(NULL);
		$em->flush();*/
		
		/*$company->setStripeCustomerId("cus_5wGhEMCRRN7Gey")
				->setStripePlanId('1')
				->setStripeCardId("card_15musUIsrpqIOiiZ9MamoalD")
				->setStripeSubscriptionId("sub_5wJdtIG32DZA60");
		$em->flush();*/
	
		$data = $request->request->all();        //Get form data
	    if (count($data)>0) { 
	        $stripeToken = $data['stripeToken'];
			
			$em = $this->getDoctrine()->getManager();          // call doctrine entity manager
			$invoice = $em->getRepository('DrillsightOrdersBundle:Invoice')->findInvoice($invoiceId); 
			$company = $invoice->getCompany();
			/*$company->setStripeCardId($stripeToken);
			$em->flush();*/

			return $this->render('DrillsightStripeBundle:Index:confirm.html.twig', array(
            	'invoiceId' => $invoiceId, 'stripeToken'=>$stripeToken
        	));
	    }
		
        return $this->render('DrillsightStripeBundle:Index:pay.html.twig', array(
        	'message' => $message, 'payableAmount' => $payableAmount, 'companyName' => $companyName
    	));

    }

	
	
	/**
     * Confirm
     *
     * This function displays the confirmation page 
     *
     * @return Response
     */
    public function confirmAction() {

        $message = '';
		
        return $this->render('DrillsightStripeBundle:Index:confirm.html.twig', array(
            'message' => $message,
        ));

    }

	/**
     * Complete
     *
     * This function 
	 * - Creates a customer in Stripe and saves the Customer ID and Card Token
	 * - Makes a charge for the payment amount in Stripe and updates the invoice entity
	 * - Creates a subscription in Stripe for the customer
     *
     * @param $stripeToken, $payableAmount, $invoiceId
     * @return Response
     */
    public function completeAction($invoiceId, $stripeToken) {
		//Use the Helper service
		$stripe_helper = $this->get('stripe_helper');
		
		$em = $this->getDoctrine()->getManager();
        $invoice = $em->getRepository('DrillsightOrdersBundle:Invoice')->findInvoice($invoiceId); 
		$company = $invoice->getCompany();
		$companyId = $company->getId();
		$trialPeriod = 0;

        $message = '';
	
		//$result = $this->createSubscriptionPlan(1);
		$result = $stripe_helper->payInvoice($company, $amount=13, $force=false);
		if (is_array($result)) {
			if (array_key_exists('error', $result)) {
				$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
				return $this->redirect($this->generateUrl('_customer_stripe_pay',array('invoiceId' => $invoiceId, )));	
			}	
		}
		else {
			$result = $stripe_helper->createSubscriptionPlan($company, $amount=490, $free_trial_period=0);
			if (is_array($result)) {
				if (array_key_exists('error', $result)) {
					$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
					return $this->redirect($this->generateUrl('_customer_stripe_pay',array('invoiceId' => $invoiceId, )));	
				}	
			}
			else {
				$stipeCustomerId = $company->getStripeCustomerId();
				$stripeCardId = $company->getStripeCardId();	
				$stipePlanId = $company->getStripePlanId();
				
				$this->get('session')->getFlashBag()->add('notice', 'Payment of invoice and subscription is complete!');
				return $this->render('DrillsightStripeBundle:Index:complete.html.twig', array(
		            'message' => $message, 'stipeCustomerId' => $stipeCustomerId, 'stripeCardId' => $stripeCardId, 'planId' => $stipePlanId,
		        ));	
			}
		}			        

    }

	/**
     * Cancel
     *
     * This is a test function for development
     *
     * @param $portal
     * @return Response
     */
    public function cancelAction($companyId) {
    	//Use the Helper service
		$stripe_helper = $this->get('stripe_helper');
		
		$result = $stripe_helper->cancelSubscription($companyId);
		if (is_array($result)) {
			if (array_key_exists('error', $result)) {
				$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
			}	
		}
		else {
			$this->get('session')->getFlashBag()->add('notice', 'Subscription was successfully cancelled!');
		}

        $message = 'Cancel Subscription';
		
        return $this->render('DrillsightStripeBundle:Index:cancel.html.twig', array(
            'message' => $message,
        ));

    }
	
	/**
     * Card
     *
     * This is a test function for development
     *
     * @param $portal
     * @return Response
     */
    public function cardAction($companyId) {
    	//Use the Helper service
		$stripe_helper = $this->get('stripe_helper');
		
		$result = $stripe_helper->getDefaultCard($companyId);

		if (array_key_exists('error', $result)) {
			$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
		}	
		else {
			$last4 = $result['last4'];
			$exp_month = $result['exp_month'];
			$exp_year = $result['exp_year'];
			$this->get('session')->getFlashBag()->add('notice', 'Creditcard was successfully retrieved!');
		}
        
		
        return $this->render('DrillsightStripeBundle:Index:card.html.twig', array(
            'last4' => $last4, 'exp_month' => $exp_month, 'exp_year' => $exp_year, 
        ));

    }


}