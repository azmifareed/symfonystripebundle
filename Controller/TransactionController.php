<?php
namespace Drillsight\StripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Drillsight\SystemBundle\Entity\Link;

class TransactionController extends Controller
{

/**
     * List Charges 
     *
     * This is a test function retrieves the list of charges in stripe for the compnay and displays them
     *
     * @param $portal
 	 * @param $companyId - Company ID
     * @return Response
     */
    public function indexAction($portal, $companyId) {
		// Add Links to content header
		$links = new ArrayCollection();
        $links->add(new Link("Back", "_".$portal."_company_view", array('id'=>$companyId),NULL,NULL,NULL,"ic_action_back_small.png"));
		
		
		/*
		 * This code is to get the list of transactions directly from Stripe
		 * 
		$stripe_helper = $this->get('stripe_helper');
		$em = $this->getDoctrine()->getManager();
	    $company = $em->getRepository('DrillsightCompanyBundle:Company')->findCompany($companyId); 
		$stripeCustomerId = $company->getStripeCustomerId();
		
		if(!$stripeCustomerId)  //If the there's no Stripe Customer ID for company return with error
		{
			$this->get('session')->getFlashBag()->add('error', "Sorry! There are no records of this company in Stripe!");
			return $this->redirect($this->generateUrl('_admin_company_view', array(
	            'id' => $companyId,
	        )));
		}
		
		//Get the list of charges for the customer in Stripe
		$result = $stripe_helper->getChargesForCustomer($stripeCustomerId, $limit=100);
		if (array_key_exists('error', $result)) 
		{
			$this->get('session')->getFlashBag()->add('error', $result['error']['message']);
			return $this->render('DrillsightStripeBundle:Transaction:index.html.twig', array(
	            'charges' => NULL, 'companyId' => $companyId, 'links' => $links, 
	        ));
		}
		else {
			$charges = $result['data'];
		}*/
		
		//Get the list of transactions from Transactions table in Drillsight
		$em = $this->getDoctrine()->getManager();
		$charges = $em->getRepository('DrillsightStripeBundle:Transaction')->findTransactions(array('company' => $companyId));
		
		
        return $this->render('DrillsightStripeBundle:Transaction:index.html.twig', array(
            'charges' => $charges, 'companyId' => $companyId, 'links' => $links, 
        ));

    }

	/**
     * View Transaction Details
     *
     * This function retreives transaction details from Stripe and displays in the view
     *
     * @param $portal
	 * @param $id - Transaction ID in Stripe
	 * @param $companyId - Company ID
     * @return Response
     */
    public function viewAction($portal,  $id, $companyId) {
		// Add Links to content header
		$links = new ArrayCollection();
        $links->add(new Link("Back", "_".$portal."_stripe_charges", array('companyId'=>$companyId),NULL,NULL,NULL,"ic_action_back_small.png"));
		
		
		$stripe_helper = $this->get('stripe_helper');
		$transaction = $stripe_helper->getTransaction($id);
		if (array_key_exists('error', $transaction)) {
			$this->get('session')->getFlashBag()->add('error', $transaction['error']['message']);
			return $this->render('DrillsightStripeBundle:Transaction:view.html.twig', array(
	            'transaction' => NULL, 'companyId' => $companyId
	        ));
		}

		//Get charge details from Stripe
		$charge = $stripe_helper->getCharge($transaction['source']);
		if (array_key_exists('error', $charge)) {		//If Stripe returns an error display error
			$this->get('session')->getFlashBag()->add('error', $charge['error']['message']);
		}
		
		//Get refund details for the charge from Stripe
		$refunds = $stripe_helper->getRefunds($transaction['source']);
		if (array_key_exists('error', $refunds)) {		//If Stripe returns an error display error
			$this->get('session')->getFlashBag()->add('error', $refunds['error']['message']);
		}
		
        return $this->render('DrillsightStripeBundle:Transaction:view.html.twig', array(
            'transaction' => $transaction, 'refunds' => $refunds, 'charge' => $charge, 'companyId' => $companyId, 'links' => $links
        ));

    }
	
}
	
	